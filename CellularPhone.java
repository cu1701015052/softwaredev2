public class CellularPhone {
  
  String telephoneNumber;
  int batteryRemaining = 100;
  
  public CellularPhone(String telNum) {
    telephoneNumber = telNum;
  }
  
  public void receiveCall(String caller) {
    System.out.println("リン！リン！リン！");
    System.out.println(caller + " から" + telephoneNumber + " への着信");
  }
  
  public void talkSomething(String msg) {
    System.out.println("[" + msg + "]");
  }
  
  public void hungUp() {
    System.out.println("通話終了");
    batteryRemaining = batteryRemaining - 10;
    System.out.println("バッテリー残量 = " +
                       batteryRemaining);
  }
  
}
